package win.tbs.springcloud.alibaba.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;


@Configuration
@MapperScan("win.tbs.springcloud.alibaba.dao")
public class MyBatisConfig {
}
