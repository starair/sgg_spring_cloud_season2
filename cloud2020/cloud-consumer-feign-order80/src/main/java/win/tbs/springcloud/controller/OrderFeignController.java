package win.tbs.springcloud.controller;

import win.tbs.springcloud.entities.CommonResult;
import win.tbs.springcloud.entities.Payment;
import win.tbs.springcloud.service.PaymentFeignService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class OrderFeignController {

    @Resource
    private PaymentFeignService paymentFeignService;

    @GetMapping(value = "/consumer/payment/get/{id}")
    public CommonResult<Payment> getPaymentById(@PathVariable("id") Long id){
        return paymentFeignService.getPaymentById(id);
    }

    @GetMapping(value = "/consumer/payment/feign/timeout")
    public String paymentFeignTimeout(){
        //OpenFeign默认等待一秒钟，超过后报错
        return paymentFeignService.paymentFeignTimeout();
    }




}


