package win.tbs.springcloud.service.impl;

import org.springframework.stereotype.Service;
import win.tbs.springcloud.dao.PaymentDao;
import win.tbs.springcloud.entities.Payment;
import win.tbs.springcloud.service.PaymentService;

import javax.annotation.Resource;

@Service
public class PaymentServiceImpl implements PaymentService {

    @Resource
    private PaymentDao paymentDao;

    public int create(Payment payment){
        return paymentDao.create(payment);
    }

    public Payment getPaymentById( Long id){

        return paymentDao.getPaymentById(id);

    }
}
