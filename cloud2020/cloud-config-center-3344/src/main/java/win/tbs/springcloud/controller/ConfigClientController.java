package win.tbs.springcloud.controller;


import cn.hutool.http.HttpUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
public class ConfigClientController {

    @GetMapping("/test")
    public String getConfigInfo(){
        return "test";
    }


    @GetMapping("/get2post/actuator/refresh")
    public String get2postActuatorRefresh(){
//        restTemplate.getForEntity("http://localhost:3355/actuator/refresh");
//        restTemplate.getForEntity("http://www.baidu.com",null);  //ok
//        restTemplate.postForEntity("http://www.baidu.com",null,null); //ok
//        restTemplate.postForEntity("http://localhost:3355/actuator/refresh",null,null);  //fail

        HttpUtil.createPost("http://localhost:3355/actuator/refresh").execute();  //没报错但也没效果.....

        return "done"+new Date();
    }

}



