package win.tbs.springcloud.alibaba.service;

import win.tbs.springcloud.alibaba.domain.Order;

public interface OrderService {
    void create(Order order);
}
